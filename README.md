## IDE

Projeto desenvolvimento no IDE Eclipse

## MAIN

A classe main do projeto para executar se encontra em /src/quiz/Quiz.java

## BIBLIOTECAS

Os arquivos das bibliotecas utilizadas se encontra em /libraries/xstream

## DADOS

As questões ficam na pasta /data/questions.xml

## GIT

https://bitbucket.org/lmachadosantos/quiz-atividade-faculdade.git

## DESENVOLVEDOR

Leandro Machado dos Santos
RA: 3115101678
