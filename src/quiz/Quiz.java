package quiz;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

public class Quiz {
    
    private static int pointsPerHit = 1;
    
    private static int hits = 0;
    
    private ArrayList<Question> questions = new ArrayList<Question>();

    private static Scanner scan;
    
    public ArrayList<Question> getQuestions() {
        return questions;
    }
    
    public static void main(String[] args) {
	
	try {
	    FileInputStream in = new FileInputStream(new File("data/questions.xml").getCanonicalPath());

	    XStream xStream = new XStream(new DomDriver());
	    xStream.addImplicitCollection(Quiz.class, "questions");
	    xStream.alias("questions", Quiz.class);
	    xStream.alias("question", Question.class);
	    xStream.alias("alternative", String.class);

	    Quiz quiz = (Quiz) xStream.fromXML(in);
	    
	    scan = new Scanner(System.in);
	    
	    System.out.println("Digite seu nome: ");
	    String name = scan.nextLine();
	    
	    Player player = new Player();
	    player.setId(1);
	    player.setName(name);
	    
	    if( player.getName().length() > 0 ){
		
		for( Question question : quiz.getQuestions() )
		{
		    System.out.println(question.getStatement());
    			
    		    System.out.println("A: " + question.getAlternatives()[0]);
    		    System.out.println("B: " + question.getAlternatives()[1]);
    		    System.out.println("C: " + question.getAlternatives()[2]);
    		    System.out.println("D: " + question.getAlternatives()[3]);
    		    System.out.println("E: " + question.getAlternatives()[4]);
    		    
    		    System.out.println("Digite a letra da alternativa correta: ");
    		    String answer = scan.next();
    		    
    		    int selectedAlternative = quiz.getSelectedAlternative(answer);
    		    
    		    if(selectedAlternative == question.getCorrect_alternative()){
    			Quiz.hits++;
    		    }
		}
		
		System.out.println();
		System.out.println(player.getName() + " você acertou " + Quiz.hits + " de 10 alternativas");
		System.out.println("Sua pontuação é " + (Quiz.pointsPerHit * Quiz.hits));
	    }
	    

	} catch (IOException ex) {
	    System.out.println("Erro ao ler arquivo!");
	}
    }
    
    public int getSelectedAlternative(String letter) {
	int number;
	switch (letter) {
		case "A": case "a":
		    number = 0;
		    break;
		case "B": case "b":
		    number = 1;
		    break;
		case "C": case "c":
		    number = 2;
		    break;
		case "D": case "d":
		    number = 3;
		    break;
		case "E": case "e":
		    number = 4;
		    break;
		default:
		    number = 5;
	}
	return number;
    }

}
