package quiz;

public class Question {
    private int id;

    private String statement;

    private String[] alternatives;

    private int correct_alternative;

    public int getId() {
	return id;
    }

    public void setId(int id) {
	this.id = id;
    }

    public String getStatement() {
	return statement;
    }

    public void setStatement(String statement) {
	this.statement = statement;
    }

    public String[] getAlternatives() {
	return alternatives;
    }

    public void setAlternatives(String[] alternatives) {
	this.alternatives = alternatives;
    }

    public int getCorrect_alternative() {
	return correct_alternative;
    }

    public void setCorrect_alternative(int correct_alternative) {
	this.correct_alternative = correct_alternative;
    }
}
